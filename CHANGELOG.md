## [1.0.2](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/compare/v1.0.1...v1.0.2) (2021-08-30)


### Bug Fixes

* make it work ([47d53c8](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/47d53c8a14e8fa2788f16c355a87a0710b2b97b4))

## [1.0.1](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/compare/v1.0.0...v1.0.1) (2021-08-30)


### Bug Fixes

* disable commitlint rules ([c3045cf](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c3045cfaaaa9ac2fe62d27841c8af6b1374200c4))
* disable dependencies ([c2ac275](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c2ac275afda8d1b3db46e0640253a716da0dfac3))
* move token to the root ([1a45bd8](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/1a45bd82054694ff718426344d2408fd6b51d537))

# 1.0.0 (2021-08-30)


### Bug Fixes

* add cypress' image to run e2e tests ([0c9fb07](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/0c9fb07acfdb5fad041c8705bc0fafe6ccab986b))
* adjust pipeline ([f254cc9](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f254cc972bd651ed51d9bbb3bf9fc13187b14498))
* adjust pipeline ([6031d8e](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/6031d8ed582c239391b30767559a0fa0463ec01b))
* cache cypress binay ([e13e9fe](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/e13e9fe404f77da065fb42011fffe6c116c25683))
* deploy command ([9e16ccf](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/9e16ccf65c81f6b8f3b6eb23ec6e0969f5218fee))
* install semantic release dependencies manually ([c7e885a](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c7e885ad91a9f8af617e4a0bb4735bbac9b4170d))
* rename npm scripts ([44e6ecc](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/44e6ecc3c530acd39fd9192603c4fbd42793ecb6))
* replace ci container's image ([7ab2cb4](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/7ab2cb4c7efc7cd8dbd41aaa7983c22ed10f7b5e))
* run release from node:14:17.5 ([f41bd16](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f41bd1634dcecbbee12c3a5839f3ba5a63ece840))
* use smaller image ([ebf7be3](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/ebf7be3d7e7afe18003ee94dd8f5412cd6e4809f))


### Features

* add fonts and typography sass file ([526ec45](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/526ec459680e93b88aecbf12dd8cef5854db62fb))
* set application skeleton ([c293155](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c293155f5518a51dcb5d4de433c7a8a90bee15fa))

# 1.0.0 (2021-08-30)


### Bug Fixes

* add cypress' image to run e2e tests ([0c9fb07](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/0c9fb07acfdb5fad041c8705bc0fafe6ccab986b))
* adjust pipeline ([f254cc9](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f254cc972bd651ed51d9bbb3bf9fc13187b14498))
* adjust pipeline ([6031d8e](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/6031d8ed582c239391b30767559a0fa0463ec01b))
* cache cypress binay ([e13e9fe](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/e13e9fe404f77da065fb42011fffe6c116c25683))
* deploy command ([9e16ccf](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/9e16ccf65c81f6b8f3b6eb23ec6e0969f5218fee))
* install semantic release dependencies manually ([c7e885a](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c7e885ad91a9f8af617e4a0bb4735bbac9b4170d))
* rename npm scripts ([44e6ecc](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/44e6ecc3c530acd39fd9192603c4fbd42793ecb6))
* replace ci container's image ([7ab2cb4](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/7ab2cb4c7efc7cd8dbd41aaa7983c22ed10f7b5e))
* run release from node:14:17.5 ([f41bd16](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f41bd1634dcecbbee12c3a5839f3ba5a63ece840))
* use smaller image ([ebf7be3](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/ebf7be3d7e7afe18003ee94dd8f5412cd6e4809f))


### Features

* add fonts and typography sass file ([526ec45](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/526ec459680e93b88aecbf12dd8cef5854db62fb))
* set application skeleton ([c293155](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c293155f5518a51dcb5d4de433c7a8a90bee15fa))

# 1.0.0 (2021-08-30)


### Bug Fixes

* add cypress' image to run e2e tests ([0c9fb07](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/0c9fb07acfdb5fad041c8705bc0fafe6ccab986b))
* adjust pipeline ([f254cc9](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f254cc972bd651ed51d9bbb3bf9fc13187b14498))
* adjust pipeline ([6031d8e](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/6031d8ed582c239391b30767559a0fa0463ec01b))
* cache cypress binay ([e13e9fe](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/e13e9fe404f77da065fb42011fffe6c116c25683))
* deploy command ([9e16ccf](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/9e16ccf65c81f6b8f3b6eb23ec6e0969f5218fee))
* install semantic release dependencies manually ([c7e885a](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c7e885ad91a9f8af617e4a0bb4735bbac9b4170d))
* rename npm scripts ([44e6ecc](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/44e6ecc3c530acd39fd9192603c4fbd42793ecb6))
* replace ci container's image ([7ab2cb4](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/7ab2cb4c7efc7cd8dbd41aaa7983c22ed10f7b5e))
* run release from node:14:17.5 ([f41bd16](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f41bd1634dcecbbee12c3a5839f3ba5a63ece840))
* use smaller image ([ebf7be3](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/ebf7be3d7e7afe18003ee94dd8f5412cd6e4809f))


### Features

* add fonts and typography sass file ([526ec45](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/526ec459680e93b88aecbf12dd8cef5854db62fb))
* set application skeleton ([c293155](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c293155f5518a51dcb5d4de433c7a8a90bee15fa))

# 1.0.0 (2021-08-30)


### Bug Fixes

* add cypress' image to run e2e tests ([0c9fb07](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/0c9fb07acfdb5fad041c8705bc0fafe6ccab986b))
* adjust pipeline ([f254cc9](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f254cc972bd651ed51d9bbb3bf9fc13187b14498))
* adjust pipeline ([6031d8e](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/6031d8ed582c239391b30767559a0fa0463ec01b))
* cache cypress binay ([e13e9fe](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/e13e9fe404f77da065fb42011fffe6c116c25683))
* deploy command ([9e16ccf](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/9e16ccf65c81f6b8f3b6eb23ec6e0969f5218fee))
* install semantic release dependencies manually ([c7e885a](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c7e885ad91a9f8af617e4a0bb4735bbac9b4170d))
* rename npm scripts ([44e6ecc](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/44e6ecc3c530acd39fd9192603c4fbd42793ecb6))
* replace ci container's image ([7ab2cb4](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/7ab2cb4c7efc7cd8dbd41aaa7983c22ed10f7b5e))
* run release from node:14:17.5 ([f41bd16](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f41bd1634dcecbbee12c3a5839f3ba5a63ece840))
* use smaller image ([ebf7be3](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/ebf7be3d7e7afe18003ee94dd8f5412cd6e4809f))


### Features

* add fonts and typography sass file ([526ec45](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/526ec459680e93b88aecbf12dd8cef5854db62fb))
* set application skeleton ([c293155](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c293155f5518a51dcb5d4de433c7a8a90bee15fa))

# 1.0.0 (2021-08-30)


### Bug Fixes

* add cypress' image to run e2e tests ([0c9fb07](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/0c9fb07acfdb5fad041c8705bc0fafe6ccab986b))
* adjust pipeline ([f254cc9](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f254cc972bd651ed51d9bbb3bf9fc13187b14498))
* adjust pipeline ([6031d8e](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/6031d8ed582c239391b30767559a0fa0463ec01b))
* cache cypress binay ([e13e9fe](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/e13e9fe404f77da065fb42011fffe6c116c25683))
* deploy command ([9e16ccf](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/9e16ccf65c81f6b8f3b6eb23ec6e0969f5218fee))
* install semantic release dependencies manually ([c7e885a](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c7e885ad91a9f8af617e4a0bb4735bbac9b4170d))
* rename npm scripts ([44e6ecc](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/44e6ecc3c530acd39fd9192603c4fbd42793ecb6))
* replace ci container's image ([7ab2cb4](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/7ab2cb4c7efc7cd8dbd41aaa7983c22ed10f7b5e))
* run release from node:14:17.5 ([f41bd16](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/f41bd1634dcecbbee12c3a5839f3ba5a63ece840))
* use smaller image ([ebf7be3](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/ebf7be3d7e7afe18003ee94dd8f5412cd6e4809f))


### Features

* add fonts and typography sass file ([526ec45](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/526ec459680e93b88aecbf12dd8cef5854db62fb))
* set application skeleton ([c293155](https://gitlab.com/firmino.changani/coingecko-defi-copycat-vue/commit/c293155f5518a51dcb5d4de433c7a8a90bee15fa))
